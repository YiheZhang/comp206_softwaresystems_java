#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "Producer.h"
#include "Consumer.h"

   // TEAM MEMEBER: 
   // Yihe Zhang : 260738383  
   // Yue  Xiao  : 260740996 


int main(){

    //creates a file named TURN.txt, and puts 0 to this file
    FILE *turn = fopen("TURN.txt", "wt");
    fprintf(turn, "%c", '0');
    fclose(turn);

    int pid = fork();
    if (pid == -1){
        printf("fork failed");
        exit(1);
    }
    //if pid = 0, call producer
    if(pid == 0){
        producer();
    }

    //if pid = 1, call consumer
    if(pid != 0){
        consumer();
    }

    return 0;
}
   
//Yihe Zhang

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

void consumer(){

    //a variable to check if turn equals to 0
    char turnChecker;
    char data;
    FILE *turnFile;
    FILE *dataFile;

    while(1){
        do{
            //opens trun.txt, if it does not exist, return
            if ((turnFile = fopen("TURN.txt", "rt")) == NULL){
                    return;
            }
            turnChecker = fgetc(turnFile);
            fclose(turnFile);
            //wait for the producer
            usleep(10000);
        }while(turnChecker=='0');
        //the function continues iff the turn is 0

        //opens data.txt and reads one char from this file
        dataFile = fopen("DATA.txt", "rt");
        data = fgetc(dataFile);
        //prints the char and closes the file
        printf("%c", data);
        fclose(dataFile);

        //overwrites TURN.txt with 0
        turnFile = fopen("TURN.txt", "w");
        fprintf(turnFile, "%c", '0');
        fclose(turnFile);
    }
}


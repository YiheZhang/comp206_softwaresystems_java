#include<stdio.h>

int main(){

    //declare variables
    char sentence[100];
    int count = 0;
    int key;
    char c;


    printf("Sentence: ");

    //add every char to sentence array
    while((c=getchar())!='\n'){
        sentence[count] = c;
        count++;
    }


    //get the key from the user
    printf("key: ");
    scanf("%d", &key);

    if(key<1||key>25){
        printf("Invalid key");
        return 0;
    }


    //Print the original sentence
    printf("Original message: ");
    int i;
    for(i=0;i<count;i++){
        printf("%c", sentence[i]);
    }


    //Encryption
    printf("\nEncrypted message: ");
    int tempchar;
    for(i=0;i<count;i++){
        tempchar = sentence[i];
        //check if the input is a letter
        if(tempchar>=65&&tempchar<=90){
            tempchar = (tempchar-65-key+26)%26+65;
        }
        if(tempchar>=97&&tempchar<=122){
            tempchar = (tempchar-97-key+26)%26+97;
        }
        printf("%c",tempchar);
        sentence[i] = tempchar;
    }


    //Decryption
    printf("\nDecrypted message: ");
    for(i=0;i<count;i++){
        tempchar = sentence[i];
        //check if the input is a letter
        if(tempchar>=65&&tempchar<=90){
            tempchar = (tempchar-65+key)%26+65;
        }
        if(tempchar>=97&&tempchar<=122){
            tempchar = (tempchar-97+key)%26+97;
        }
        printf("%c",tempchar);
    }
    printf("\n");

    return 0;
}

#ifndef _LIST_H_
#define _LIST_H_



struct NODE{
    int data;
    struct NODE *next;
};
void newList();
int addNode(int value);
void prettyPrint();
#endif

#include <stdio.h>
#include <stdlib.h>
#include "list.h"


///////////////////////////////////
#define TRUE 1
#define FALSE 0
struct NODE *head;

//a global variable to check whether if the linked list is initialized
int check = FALSE;


void newList(){
    head = NULL;
    //initialize a linked list
    check = TRUE;
}

int addNode(int value){
    if (check==TRUE){
        //create a new node
        struct NODE *newNODE = (struct NODE *) malloc(sizeof(struct NODE));
        //assign data and pointer to the new node
        (*newNODE).next = head;
        (*newNODE).data = value;
        //now head points to the node just added
        head = newNODE;
        if(head==NULL) return FALSE;
        return TRUE;
    }
    return FALSE;
}

void prettyPrint(){
    struct NODE *temp;
    temp = head;
    //if head does not point to anything, exit
    if (temp==NULL) exit(0);
    //a loop to print the linked list
    while((*temp).next!=NULL){
        printf("%d => ", (*temp).data);
        temp = (*temp).next;
    }
    printf("%d\n", (*temp).data);
}




#include <stdio.h>
#include <stdlib.h>
#include "list.h"

int main(){

    newList();
    //enter value
    int i, error1=1, error2=1;
    while(error1==1||error2==1){
        printf("Enter an integer:");
        error1 = scanf("%d", &i);
        //if integer is negative, exit
        if(i<0){
            break;
        }
        //if scanf returned 0, exit
        if(error1==0){
            printf("Wrong type\n");
            exit(0);
        }
        //addNode, if it fails, exit
        error2 = addNode(i);
        if(error2==0){
            printf("Failed to add a node\n");
            exit(0);
        }
    }
    prettyPrint();
    return 0;
}

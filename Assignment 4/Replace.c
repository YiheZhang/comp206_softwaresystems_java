#include <stdio.h>
#include <stdlib.h>



void FindRecord(char *filename, char *name, char record[]){

    FILE *file = fopen(filename,"rt");
    char str[1000];
    int nameLength = 0;

    //Get the length of the name
    while(name[nameLength] != '\0'){nameLength++;}
    //printf("NameLength: %d\n",nameLength);

    if(file == NULL){
        printf("Unable to find %s\n", filename);
    }else{
        int true = 1;   //a variable to end the while loop
        while(fgets(str,1000,file)&&true==1){
            char *strAddress = str;         //Get the address of the string

            //Count = length of the name from .csv
            int count = 0;
            int same = 1;   //find out if the input name and name from csv are the same
            while((*strAddress != ',')&&same==1){
                if(*strAddress!=name[count]){
                    same=0;
                }
                count++;
                *strAddress++;
            }

            //if two name's length and chars are the same, end the while loop
            char *strAddress2 = str;    //Get the address of string, copy data to record.
            if(count==nameLength&&same==1){
                while (*strAddress2){
                    *record = *strAddress2;
                    *record++;
                    *strAddress2++;
                }
                true = 0;
            }
        }
        if(true==1)printf("input name does not match\n");
    }
    fclose(file);
}


void Replace(char *filename, char *newName, char record[]){
    FILE *file = fopen(filename,"rt");
    char str[1000];

    //get the length of the name
    int nameLength = 0;
    while(*record!=','){
        *record++;
        nameLength++;
    }
    //decrement the pointer
    int i = nameLength;
    while(i!=0){
        *record--;
        i--;
    }
    //get the name
    char name[nameLength];
    char *nameAddress = name;
    while(*record!=','){
        *nameAddress = *record;
        *nameAddress++;
        *record++;
    }
    //printf("%s\n",name);

    if(file == NULL){
        //printf("Unable to find %s\n", filename);
    }else{
        int true = 1;   //a variable to end the while loop
        while(fgets(str,1000,file)&&true==1){
            char *strAddress = str;     //Get the address of the string
            char *nameAddress2 = name;  //Get the name address;
            int count = 0;
            int same = 1;
            while((*strAddress != ',')&&same==1){
                if(*strAddress!=*nameAddress2){
                    same=0;
                }
                count++;
                *strAddress++;
                *nameAddress2++;
            }

            //if two name's length and chars are the same, end the while loop
            if(count==nameLength&&same==1){
                char tempStr[1000];     //Create a temp string, and get its address
                char *tempStrAdd = tempStr;
                char *tempStrAdd2 = tempStr;
                char *newNameAdd = newName;     //get the address of new name
                //Add newName to temp String
                while(*newNameAdd!='\0'){
                    *tempStrAdd = *newNameAdd;
                    *tempStrAdd++;
                    *newNameAdd++;
                }
                //Replace the original name with new name
                while(*strAddress){
                    *tempStrAdd = *strAddress;
                    *tempStrAdd++;
                    *strAddress++;
                }
                //decrement the pointer
                int i = nameLength;
                while(i!=0){
                    *record--;
                    i--;
                }

                //Copy everything of tempStr to record
                while (*tempStrAdd2||*record){
                    *record = *tempStrAdd2;
                    *record++;
                    *tempStrAdd2++;
                }
            }
        }
        //if(true==1)printf("Unable to Find a Match Name\n");
    }
    fclose(file);
}


void SaveRecord(char *filename, char *name, char record[]){

    FILE *file = fopen(filename,"rt");
    //FILE *tempFile = fopen("tempFile.csv", "wt");

    char str[1000];
    int nameLength = 0;
    //Get the length of the name
    while(name[nameLength] != '\0'){nameLength++;}

    if(file == NULL){
        //printf("Unable to find %s\n", filename);
    }else{
        FILE *tempFile = fopen("tempFile.csv", "wt");

        while(fgets(str,1000,file)){
            char *strAddress = str;         //Get the address of the string
            //Count = length of the name from .csv
            int count = 0;
            int same = 1;   //find out if the input name and name from csv are the same
            while((*strAddress != ',')&&same==1){
                if(*strAddress!=name[count]){
                    same=0;
                }
                count++;
                *strAddress++;
            }

            if(count==nameLength&&same==1){     //Replace the row with record
                fputs(record, tempFile);
            }
            else{                               //copy every rows to tempFile
                fputs(str, tempFile);
            }
        }
    fclose(file);       //close with the original file
    fclose(tempFile);   //close the temp file

    remove(filename);   //remove the original file
    rename("tempFile.csv", filename);   //change the tempfile's name to filename
    }
}


int main()
{
    //Declare variables including the filename
    char name[100];
    char newName[100];
    char record[1000];

    char filename[] = "Phonebook.csv";

    //Get the name and the replacement name
    printf("Enter a Name: ");
    scanf("%s", name);
    printf("Enter a Replacement Name: ");
    scanf("%s", newName);

    //using FindRecord, get the record array
    FindRecord(filename, name, record);

    //Replace record with new name
    Replace(filename, newName, record);

    //Save record
    SaveRecord(filename, name, record);

    return 0;
}
